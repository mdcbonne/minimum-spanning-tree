
/*****************************************
 * Instructions
 *  - Replace 'uwuserid' with your uWaterloo User ID
 *  - Select the current calendar term and enter the year
 *  - List students with whom you had discussions and who helped you
 *
 * uWaterloo User ID:  mdcbonne@uwaterloo.ca
 * Submitted for ECE 250
 * Department of Electrical and Computer Engineering
 * University of Waterloo
 * Calender Term of Submission:  Fall 2018
 *
 * By submitting this file, I affirm that
 * I am the author of all modifications to
 * the provided code.
 *
 * The following is a list of uWaterloo User IDs of those students
 * I had discussions with in preparing this project:
 *    -
 *
 * The following is a list of uWaterloo User IDs of those students
 * who helped me with this project (describe their help; e.g., debugging):
 *    -
 *****************************************/

#ifndef WEIGHTED_GRAPH_H
#define WEIGHTED_GRAPH_H

#ifndef nullptr
#define nullptr 0
#endif

#include <iostream>
#include <limits>
#include "Exception.h"
#include "Disjoint_sets.h"
#include <vector>

using namespace std;

class Weighted_graph
{
  private:
	static const double INF;
	double **graph;
	int *degree_;
	int num_nodes;
	int num_edges;

	// Do not implement these functions!
	// By making these private and not implementing them, any attempt
	// to make copies or assignments will result in errors
	Weighted_graph(Weighted_graph const &);
	Weighted_graph &operator=(Weighted_graph);

	// your choice

  public:
	Weighted_graph(int = 10); // Initialize a new graph, specifying the number of vertices it should have (default: 10)
	~Weighted_graph(); // Destructor for the Weigted_graph class

	int degree(int) const; // Return the degree of a given vector
	int edge_count() const; // Return the number of edges in the graph
	std::pair<double, int> minimum_spanning_tree() const; // Find the graph's minimum spanning tree and return its total weight and the number of edges tested to find it

	bool insert_edge(int, int, double); // Insert an edge into the graph
	bool erase_edge(int, int); // Erase an edge from the graph
	void clear_edges(); // Clear all the edges from the graph

	// Friends

	friend std::ostream &operator<<(std::ostream &, Weighted_graph const &);
};

const double Weighted_graph::INF = std::numeric_limits<double>::infinity();

Weighted_graph::Weighted_graph(int n)
{
	// If n < 0, throw illegal argument exception
	if (n < 0)
	{
		throw illegal_argument();
	}
	// Initialize the rows of the adjacency matrix
	graph = new double *[n];
	// Initialize the columns of the adjacency matrix
	for (int i = 0; i < n; i++)
	{
		graph[i] = new double[n];
		for (int j = 0; j < n; j++)
		{
			// Set initial edge weights to INF, which means no edge here
			graph[i][j] = INF;
		}
	}
	// Initialize the degree array
	degree_ = new int[n];
	for(int i = 0; i < n; i++){
		degree_[i] = 0;
	}
	// Set num_nodes to n
	num_nodes = n;
	// Set num_edges to 0
	num_edges = 0;
}

Weighted_graph::~Weighted_graph()
{
	// Delete every column in the adjacency matrix
	for (int i = 0; i < num_nodes; i++)
	{
		delete[] graph[i];
	}
	// Delete the now 1D graph array
	delete[] graph;
	// Delete the degree_ array
	delete[] degree_;
}

int Weighted_graph::degree(int u) const
{
	// If u > num_nodes or less than 0, throw illegal_argument exception
	if (u >= num_nodes || u < 0)
	{
		throw illegal_argument();
	}
	// Return the degree of the u'th node
	return degree_[u];
}

int Weighted_graph::edge_count() const
{
	// Return the number of edges
	return num_edges;
}

bool Weighted_graph::erase_edge(int i, int j)
{
	// If i or j is less than 0 or greater than num_nodes, throw illegal_argument
	if (min(i, j) < 0 || max(i, j) >= num_nodes)
	{
		throw illegal_argument();
	}

	// If i == j, return true 
	if (i == j)
	{
		return true;
	}

	// If the edge exists (ie its weight is INF), delete it and decrement the degree of its two vertices and num_edges
	if (graph[i][j] != INF)
	{
		graph[i][j] = INF;
		degree_[i]--;
		graph[j][i] = INF;
		degree_[j]--;
		num_edges--;

		return true;
	}

	return false;
}

void Weighted_graph::clear_edges()
{
	for (int i = 0; i < num_nodes; i++)
	{
		// Reset every vertex's degree to 0
		degree_[i] = 0;
		for (int j = 0; j < num_nodes; j++)
		{
			// Set every edge weight to INF
			graph[i][j] = INF;
		}
	}
	// Reset num_edges to 0
	num_edges = 0;

	return;
}

bool Weighted_graph::insert_edge(int i, int j, double d)
{
	// If i or j is less than 0 or greater than num_nodes, or if d is less than or equal to 0, throw illegal_argument
	if (min(i, j) < 0 || max(i, j) >= num_nodes || d <= 0)
	{
		throw illegal_argument();
	}
	// If i = j, return false
	if (i == j)
	{
		return false;
	}
	if(graph[i][j] == INF){ // If no edge between these two vertices existed before
		// Increment the degree of the i'th and j'th vertices
		degree_[i]++;
		degree_[j]++;
		// Increment num_edges
		num_edges++;
	}
	// Set the weight of the edge between the i'th and j'th vertices to d
	graph[i][j] = d;
	graph[j][i] = d;
	return true;
}

std::pair<double, int> Weighted_graph::minimum_spanning_tree() const
{
	// Initialize a vector of <int, int> pairs to store the sorted edges
	std::vector<std::pair<int, int> > sorted_edges;

	for(int i = 0; i < num_nodes; i++){
		for(int j = i+1; j < num_nodes; j++){
			if(graph[i][j] != INF){
				double weight = graph[i][j];
				bool inserted = false;
				for (int k = 0; k < sorted_edges.size(); k++){
					if (graph[sorted_edges[k].first][sorted_edges[k].second] >= weight){
						// If the weight of the current edge is less than or equal to the weight of the kth edge in sorted_edges, insert the current edge in the (k-1)th position
						sorted_edges.insert(sorted_edges.begin() + k, std::pair<int, int>(i, j));
						inserted = true;
						break;
					}
				}
				// If sorted_edges is empty or if the current weight is greater than the weights of all of the edges in sorted_edges, insert it at the back
				if (sorted_edges.empty() || !inserted){
					sorted_edges.push_back(std::pair<int, int>(i, j));
				}
			}
		}
	}

	int edge_test_counter = 0;
	double total_weight = 0.0;
	// Initialize a new Weighted_graph for the minimum_spanning_tree
	Weighted_graph minimum_spanning_tree(num_nodes);
	// Initialize a new disjoint set with a set for each node in the graph
	Disjoint_set vertices(num_nodes);
	// Iterate through the sorted edges from smallest to largest weight
	for (auto edge = sorted_edges.begin(); edge != sorted_edges.end(); edge = next(edge))
	{
		if (vertices.num_sets() == 1) // If the number of sets in vertices is 1, meaning all of the nodes in minimum_spanning_tree are connected, break to return
		{
			break;
		}
		int i = edge->first;
		int j = edge->second;
		if (vertices.find_set(i) != vertices.find_set(j)){ // If no edge exists already between nodes i and j in the MST, insert this edge
			double weight = graph[i][j];
			// Insert the edge
			minimum_spanning_tree.insert_edge(i, j, weight);
			// Union the sets of the i and j nodes, indicating that they are now connected
			vertices.union_sets(i, j);
			// Add the weight of the added edge to the total weight of the MST
			total_weight += weight;
		}
		// Increment edge_test_counter for every edge tested
		edge_test_counter++;
	}
	// Return the total weight of the MST and the number of edges tested
	return std::pair<double, int>(total_weight, edge_test_counter);
}

std::ostream &operator<<(std::ostream &out, Weighted_graph const &graph)
{

	out << std::endl;

	for (int i = 0; i < graph.num_nodes; i++)
	{
		out << "   " << i;
	}
	out << std::endl;

	for (int i = 0; i < graph.num_nodes; i++)
	{
		out << i << "  ";
		for (int j = 0; j < graph.num_nodes; j++)
		{
			out << graph.graph[i][j] << " ";
		}
		out << std::endl;
	}

	return out;
}

#endif
