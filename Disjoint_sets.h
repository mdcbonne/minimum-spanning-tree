
#ifndef DISJOINT_SET_H
#define DISJOINT_SET_H

#ifndef nullptr
#define nullptr 0
#endif


#include <limits>
#include "Exception.h"

using namespace std;

struct ll_entry; // represents each element of the linked list
struct set_info; // keeps track of the information (pointers to head and tail and the size of the set) of each set
//could we delete the above two lines?


struct ll_entry{ 
	int content;
	set_info* ptr_to_info; // ptr to the info entry of the corresponding set
	ll_entry* next;
	
};

struct set_info { 
	ll_entry* head;
	ll_entry* tail;
	int size;
};

class Disjoint_set {

private:
	ll_entry** nodes;
	set_info** sets;
	int set_counter;
	int initial_num_sets;
public:
	Disjoint_set(int); // Initialize a new Disjoint_set object, specifying the number of sets it should have
	~Disjoint_set(); // Destructor for the Disjoint_set class
	int find_set(int) const; // Returns an identifier for the set that a given entry belongs to
	int num_sets() const; // Returns the number of sets in the Disjoint_set object
	void union_sets(int, int); // Performs a union between two sets, adding all of the smaller set's entries to the larger set
};

Disjoint_set::Disjoint_set(int n) : 
nodes(new ll_entry*[n]), // initialize the nodes array
sets (new set_info*[n]), // initialize the sets array
set_counter(n), // Set set_counter to n, the initial number of sets
initial_num_sets(n) // Set initial_num_sets to n, the initial number of sets
{
	// initialize the sets
	for(int i = 0; i < initial_num_sets; i++){ // For every node and set
		// Initialize a new ll_entry object at nodes[i]
		nodes[i] = new ll_entry;
		// Set node[i]'s next ptr to nullptr
		nodes[i]->next = nullptr;
		// Set node[i]'s content member to i
		nodes[i]->content = i;
		// Initialize a new set_info object at sets[i]
		sets[i] = new set_info;
		// Set sets[i]'s head ptr to nodes[i]
		sets[i]->head = nodes[i];
		// Set sets[i]'s tail pointer to nodes[i]
		sets[i]->tail = nodes[i];
		// Set sets[i]'s size member to 1
		sets[i]->size = 1;
		// Set nodes[i]'s ptr_to_info ptr to sets[i]
		nodes[i]->ptr_to_info = sets[i];
	}
}

Disjoint_set::~Disjoint_set() {
	for(int i = 0; i < initial_num_sets; i++){
		// Delete the ll_entry object at nodes[i]
		delete nodes[i];
		// Delete the set_info object at sets[i]
		delete sets[i];
	}
	// Delete the nodes array
	delete[] nodes;
	// Delete the sets array
	delete[] sets;
}
int Disjoint_set::find_set(int item) const{
	// Return the content of the head node in node[item]'s set, which gives the index of nodes[item]'s set
	return nodes[item]->ptr_to_info->head->content;
}

int Disjoint_set::num_sets() const {
	// Return the set_counter member
	return set_counter;
}

void Disjoint_set::union_sets(int node_index1, int node_index2) {
	if (node_index1 == node_index2)
		// If node_index1 == node_index2, they're the same node, and thus are already in the same set, so we return
		return;
	// Assign the pointer si1 to nodes[node_index1]'s set
	set_info* si1 = nodes[node_index1]->ptr_to_info;
	// Assign the pointer si2 to nodes[node_index2]'s set
	set_info* si2 = nodes[node_index2]->ptr_to_info;

	if (si1->head->content == si2->head->content){
		// If the nodes at node_index1 and 2 are in the same set, then return
		return;
	}

	// ni1: the index of the larger set, ni2: the index of the smaller index
	int ni1 = si1->size >= si2->size ? node_index1 : node_index2;
	int ni2 = si1->size < si2->size ? node_index1 : node_index2;

	if(si1->size < si2->size){
		// If si1 is less than si2, swap the pointers so that si1 points to the larger set
		set_info* temp = si2;
		si2 = si1;
		si1 = temp;
	}


	//iterate through the smaller set and assign each of its nodes to the larger set
	ll_entry* current_node = nodes[ni2]->ptr_to_info->head;
	while (current_node!=nullptr) {
		current_node->ptr_to_info = si1;
		current_node = current_node->next;
	}

	// Set the last node in si1's next ptr to the first node in si2
	si1->tail->next = si2->head;
	// Assign si1's tail pointer to si2
	si1->tail = si2->tail;
	// Add si2's size to si1's size member
	si1->size += si2->size;
	// Decrement the set_counter to reflect the fact that we've merged two sets
	set_counter--;
	
}


#endif
